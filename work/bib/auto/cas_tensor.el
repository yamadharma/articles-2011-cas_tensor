(TeX-add-style-hook "cas_tensor"
 (lambda ()
    (LaTeX-add-bibitems
     "pfur-2012-1"
     "pfur-2011-2"
     "pfur-2011-2::en"
     "cartan"
     "penrose"
     "penrose::en"
     "cain"
     "pfur-2007-1-2"
     "pfur-2007-1-2::en"
     "ech"
     "ech::en"
     "toth"
     "sardanashvili-2"
     "mangiarotti2000connections"
     "barut:1980"
     "barut:1986::en"
     "mathtensor"
     "gerdt:1991:reduce"
     "gerdt:1980:ufn"
     "gerdt:1980:ufn::en")))

