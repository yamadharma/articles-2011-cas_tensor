(TeX-add-style-hook "cas_tensor"
 (lambda ()
    (LaTeX-add-bibliographies
     "bib/cas_tensor")
    (LaTeX-add-labels
     "eq:9"
     "eq:11"
     "eq:12"
     "eq:2"
     "eq:3"
     "eq:4"
     "eq:5"
     "eq:7"
     "eq:6"
     "eq:10"
     "eq:8"
     "eq:gamma:sym:1"
     "eq:gamma:sym:2"
     "eq:gamma:sym:3"
     "eq:gamma:sym:4"
     "eq:riman:1"
     "eq:riman:2"
     "eq:riman:3"
     "eq:13")))

